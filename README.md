0. Precautions for running software: If you download PyMieLab_ After V1.0 compresses the software and decompresses it, 
   click PyMieLab_ V1.0.exe executable program, the best resolution of this software is 1366 × 768, if the software 
   interface is not fully displayed, you can change the screen resolution or drag the mouse to adjust the right edge 
   of the software interface.

1. This software and its source code have obtained copyright certification and can be freely used for research, 
   teaching or personal use. The complete software package without modification can be redistributed and freely 
   exchanged for academic research, but cannot be commercialized or used for commercial purposes.
   
2. In any demonstration using this software package (or any other code using this software), 
   this article should be appropriately referenced: PyMieLab_ V1.0: A software for calculating the light scattering and absorption of spherical particle.
   
3. These codes have been developed and tested on Windows 7/10/11 and other operating systems. Although every 
   effort has been made to eliminate errors (programming errors or incorrect physical formulas), there may 
   still be some problems. We hope users can help us identify them and try to update the code if necessary.
   
4. The author does not assume any responsibility for improper use of the program, unexpected errors that may 
   still exist, or improper interpretation of its limitations and/or the results arising therefrom. It is the 
   responsibility of the user to check the validity of the inputs/outputs, their physical interpretation and 
   their applicability to their specific problems.